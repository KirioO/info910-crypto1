#include <cmath>
#include <iostream>
#include <fstream>
#include "src/Scoring.hpp"
#include "src/playFair.hpp"
#include <random>
#include <chrono>

std::string craque(Scoring S, std::string textChiffre, int nbCle, float tempInit, float tempPas, float tempFinal) {

    playFair PF;
    float T = tempInit;
    key prevKey;
    key testKey = prevKey = prevKey.perturbeKey(std::rand()%2000);

    int nbConsec = 0;
    while (nbConsec < 5 && T > tempFinal){
        key tempKey = testKey;
        for (int k = 0; k < nbCle; ++k) {

            tempKey = testKey.perturbeKey(1);
            float STest = S.score(PF.dechiffreText(textChiffre, testKey));
            float STemp = S.score(PF.dechiffreText(textChiffre, tempKey));
            float delta = STest - STemp;
            if (delta < 0 || ((double) std::rand() / (RAND_MAX)) < std::exp(-delta/T))
            {
                testKey = tempKey;
                if (S.score(PF.dechiffreText(textChiffre, prevKey)) <= S.score(PF.dechiffreText(textChiffre, testKey)))
                {
                    prevKey = testKey;
                    nbConsec = 0;
                }
            }
        }
        std::cout << "Temp : " << T << std::endl;
        prevKey.printKey();
        if (S.score(PF.dechiffreText(textChiffre, prevKey)) >= S.score(PF.dechiffreText(textChiffre, testKey))){
            nbConsec ++;
        }
        else{
            prevKey = testKey;
            nbConsec = 0;
        }
        std::cout << "Score Prev at change temp = " << S.score(PF.dechiffreText(textChiffre, prevKey)) << std::endl;
        std::cout << "nbConsec : " << nbConsec << std::endl;
        T -= tempPas;
    }
    std::string dechiffre = PF.dechiffreText(textChiffre, prevKey);

    std::cout << dechiffre << std::endl;
    return dechiffre;

}

int main() {
    std::srand( (std::time(nullptr) * 8253729 + 2396403 )%32768); //Set seed for pseudo random generator

    Scoring scoring("francais", 4) ;

    char keyT[5][5] = {
            {'T','Q','O','H','W'},
            {'E','I','L','C','S'},
            {'K','U','R','A','G'},
            {'D','F','V','M','N'},
            {'Y','Z','B','P','X'},
    };
    key K(keyT);

    playFair playFair;

    std::cout << "Score SALUT " << scoring.score("SALUT") << std::endl;
    std::cout << "Score ATILU " << scoring.score("ATILU") << std::endl;
    std::cout << "Score LOREMIPSUM " << scoring.score("LOREMIPSUM") << std::endl;
    std::cout << "Score QIRSOPBBMF " << scoring.score("QIRSOPBBMF") << std::endl << std::endl;


    std::string mchiffrer = playFair.chiffreText("SALUTTUVASBIENTTCESTTTTT", K);
    std::cout << "chiffrement de 'SALUTTUVASBIENTTCESTTTTT' " <<  std::endl;
    std::cout << "message chiffrer " << mchiffrer << " " << mchiffrer.size() <<  std::endl;
    std::cout << "message dechiffre " << playFair.dechiffreText(mchiffrer, K) <<  std::endl << std::endl;

    std::string mchiffrer2 = playFair.chiffreText("QIRSOPBBMF", K);
    std::cout << "chiffrement de 'QIRSOPBBMF' " <<  std::endl;
    std::cout << "messafe chiffrer" <<  mchiffrer2 << " " << mchiffrer2.size() <<  std::endl;
    std::cout << "messafe dechiffrer" << playFair.dechiffreText(mchiffrer2, K) << " " << mchiffrer2.size() <<  std::endl <<  std::endl;

    std::string test = "AMUNOFIEMSMHSECNMTSGCZIERCSNRPCZCSEGAZAOQAMOMTNGICMIFAUOHOOSNRSYZAUKOUUKTAMAELIYATWMKRIESPZACRAOQANZMTOSOURPOHMARUCACINAFTNZMEPRUOHOSCDRASURZCSNLRKHUOLNOMHQNZMERPACZCMICSSGELIYATGSDRSAUZIFOAIYRTTFICOFDNAMSGCHISHSTEFIMOCSIYASUOTEMOMTEMOGPEAHPWPKAOASOSSAEMUKOUTAAKISAZZIZMIVAMNSTINSMISYPALITAATTEPEECUDAZIAOHMOIEHRANTEUKOUCRRSHOTELNKZCSEMOSAHIWISZMCHRDFOAHTONRTIUOATNXOASGMTEIXCHSNSNTAORMINEISAACANTEUKTAAPEIIVRSHOTEMECSESIWISZMAMTMLNKZCSEMOSAHKHSECNMTOQPAOMOMRCISQRCZIERCSNHRTIENQETAUOARNTFRNSNTRGPEEIXCAOUIIDCSEMOMTEDTKZOATMRTEIMIAOMECSEMTOELRPURHTATAGHZHCTIHOHMHZLIOHMADRABTAMELITAHLXFWREMUKSAIULIMECSESFRISHTDWNUUHICNTUIDWMEDRSNNZMTOSNZOHKQRDOGMOCIATRPUOMECSEQIFAZTMDENVOMHZHTGSOAIURFNZMERUNMRPCRNSGPPAAQCWNAPWURSNABSUZAONKZMOMTOGPECZSINZMEAOUNESSNABSUZARDLDOHKQRPNENZ";
    //std::string test = "FTITNDPQDAEMTOQBCTANFCDUQNEAEACXAZMEPSNAQKMETRNBAVLZBNSECNQLNUNAOCSTMEANPINLMELCSOPDPUNDTPADTFTMRNDOEALCTCNUNAQDQLSCDT";
    //std::string test = "SALUT";
    std::cout << "craque size : " << test.size() << " text " << test <<  std::endl;
    craque(scoring, test, 50000, test.size()/8, 1, 0);

    return 0;
}