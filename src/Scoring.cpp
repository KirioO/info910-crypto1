//
// Created by quent on 13/09/2019.
//

#include <iostream>
#include <sstream>
#include <math.h>
#include "Scoring.hpp"


void getProbas(std::map<std::string, float> &scores, int total){
    for (std::pair<const std::string,float>& x: scores) {
        if (x.second == 0.0f){
            x.second = std::log(0.01) - std::log(total);
        }
        else{
            x.second = std::log(x.second) - std::log(total);
        }
    }
}

bool Scoring::readFile(std::istream & input) {
    std::string str;
    this->total = 0;
    if (!input.good()) {
        std::cerr << "Problem!";
        return false;
    }
    std::getline(input, str);
    while (!str.empty()){
        std::istringstream iss(str);
        std::string key;
        int value;
        iss >> key >> value;
        this->total += value;
        this->scores.insert(std::pair<std::string, float>(key, value ));
        std::getline(input, str);
    }
    this->valeurNull = log(0.01) - log(this->total);
    getProbas(this->scores, this->total);

    return true;
}

void Scoring::displayProba() {
    for (std::pair<const std::string,float>& x: this->scores) {
        std::cout << x.first << " => " << x.second << '\n';
    }
}

float Scoring::score(std::string chaine) {
    std::vector<std::string> grams;
    for (int i = 0; i <= chaine.size() - this->gramLength; ++i){
        std::string g = "";
        g.append(chaine.substr(i, this->gramLength));
        grams.push_back(g);
    }
    float s = 0;
    for(const auto& g: grams){
        if (this->scores.count(g) > 0)
            s += this->scores.at(g);
        else
            s += this->valeurNull;
    }
    return s;
}
