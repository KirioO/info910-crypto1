//
// Created by quent on 13/09/2019.
//

#ifndef INFO910_CRYPTO_SCORING_HPP
#define INFO910_CRYPTO_SCORING_HPP

#include <vector>
#include <map>
#include <string>
#include <istream>
#include <fstream>

class Scoring {
    int total;
    float valeurNull;

    std::string directory = "..\\grams\\";
    std::string langage;
    int gramLength;
    std::map<std::string, float> scores;
public:
    Scoring():langage("francais"), gramLength(1){
        std::ifstream input(this->directory + std::to_string(this->gramLength) + "-grams-" + this->langage + "-ALPHA.save");
        this->readFile(input);
        this->total = 0;
        this->valeurNull = 0;
    };
    Scoring(std::string _langage, int _gramLenght): langage(_langage), gramLength(_gramLenght){
        std::ifstream input(this->directory + std::to_string(this->gramLength) + "-grams-" +this->langage + "-ALPHA.save");
        this->readFile(input);
        this->total = 0;
        this->valeurNull = 0;
    }
    bool readFile(std::istream & input);
    void displayProba();
    float score(std::string chaine);

};


#endif //INFO910_CRYPTO_SCORING_HPP
