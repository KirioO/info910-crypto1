//
// Created by quent on 18/09/2019.
//

#ifndef INFO910_CRYPTO_KEY_HPP
#define INFO910_CRYPTO_KEY_HPP


#include <utility>
#include <iostream>

class key {
public:

    char ckey[5][5]{};
    char dkey[5][5]{};

    key(){
        char _key[5][5] = {
                {'A','B','C','D','E'},
                {'F','G','H','I','K'},
                {'L','M','N','O','P'},
                {'Q','R','S','T','U'},
                {'V','W','X','Y','Z'},
        };
        for (int i = 0; i < 5; ++i) {
            for (int j = 0; j < 5; ++j) {
                this->ckey[i][j] = _key[i][j];
                this->dkey[i][j] = _key[4-i][4-j];
            }
        }
    }

    key(char _key[5][5]){
        for (int i = 0; i < 5; ++i) {
            for (int j = 0; j < 5; ++j) {
                this->ckey[i][j] = _key[i][j];
                this->dkey[i][j] = _key[4-i][4-j];
            }
        }
    }


    key perturbeKey(int nbPermut);
    void printKey();

    bool operator==( const key & k2){
        bool res = true;
        for (int i = 0; i < 5; ++i) {
            for (int j = 0; j < 5; ++j) {
                if (this->ckey[i][j] != k2.ckey[i][j]){
                    res = false;
                    return res;
                }
            }
        }
        return res;
    }

};


#endif //INFO910_CRYPTO_KEY_HPP
