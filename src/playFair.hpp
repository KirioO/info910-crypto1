//
// Created by quent on 13/09/2019.
//

#ifndef INFO910_CRYPTO_PLAYFAIR_HPP
#define INFO910_CRYPTO_PLAYFAIR_HPP


#include <string>
#include <iostream>
#include "key.hpp"

class playFair {
public:


    std::string chiffrePair(std::string pair, char key[5][5]);
    std::string chiffreText(std::string text, key K);
    std::string dechiffreText(std::string text, key K);

    std::pair<int, int> findInKey(char c, char key[5][5]);

};


#endif //INFO910_CRYPTO_PLAYFAIR_HPP
