//
// Created by quent on 18/09/2019.
//

#include "key.hpp"

key key::perturbeKey(int nbPermut) {
    char perturbeKey[5][5]{};
    for (int i = 0; i < 5; ++i) {
        for (int j = 0; j < 5; ++j) {
            perturbeKey[i][j] = this->ckey[i][j];
        }
    }

    for (int i = 0; i < nbPermut; ++i) {
        int prob = rand() % 100;
        std::pair<int, int> case1(rand() % 5, rand() % 5);
        std::pair<int, int> case2(rand() % 5, rand() % 5);
        int LC1(rand() % 5);
        int LC2(rand() % 5);

        switch (prob) {
            case 0 ... 24:
                //Permute 2 colones
                while (LC1 == LC2) {
                    LC1 = rand() % 5;
                    LC2 = rand() % 5;
                }
                for (int l = 0; l < 5; ++l) {
                    std::swap(perturbeKey[l][LC1], perturbeKey[l][LC2]);
                }
                break;
            case 25 ... 49:
                //Permute 2 lignes
                while (LC1 == LC2) {
                    LC1 = rand() % 5;
                    LC2 = rand() % 5;
                }
                for (int c = 0; c < 5; ++c) {
                    std::swap(perturbeKey[LC1][c], perturbeKey[LC2][c]);
                }
                break;
            case 50 ... 100:
                //Permute 2 cases
                while (case1 == case2) {
                    case1 = std::pair<int, int>(rand() % 5, rand() % 5);
                    case2 = std::pair<int, int>(rand() % 5, rand() % 5);
                }
                std::swap(perturbeKey[case1.first][case1.second], perturbeKey[case2.first][case2.second]);
                break;
            default:
                break;

        }
    }
    return key(perturbeKey);
}

void key::printKey() {
    for(auto & i:this->dkey){
        for (char j:i){
            std::cout << j <<  " ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}



