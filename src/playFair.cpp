//
// Created by quent on 13/09/2019.
//

#include "playFair.hpp"
#include <algorithm>

std::pair<int, int> playFair::findInKey(char c, char key[5][5]) {
    for (int i = 0; i < 5; ++i) {
        for (int j = 0; j < 5; ++j) {
            if ( key[i][j] == c)
                return std::pair<int,int >(i,j);
        }
    }
    return std::pair<int ,int >(0,0);
}

std::string playFair::chiffrePair(std::string pair, char key[5][5]) {
    std::string image;
    std::string strToChiffre = pair;

    std::pair<int,int> keyCoordinate1 = findInKey(strToChiffre[0], key);
    std::pair<int,int> keyCoordinate2 = findInKey(strToChiffre[1], key);

    //Same line
    if (keyCoordinate1.first == keyCoordinate2.first){
        image += (key[keyCoordinate1.first][(keyCoordinate1.second + 1)%5 ]);
        image += (key[keyCoordinate2.first][(keyCoordinate2.second + 1)%5 ]);
        return image;
    }
    //Same colmumn
    if (keyCoordinate1.second == keyCoordinate2.second){
        image += key[(keyCoordinate1.first + 1)%5][keyCoordinate1.second];
        image += key[(keyCoordinate2.first + 1)%5][keyCoordinate2.second];
        return image;
    }
    //Normal behaviour
    std::pair<int,int> hautDroit(std::min(keyCoordinate1.first, keyCoordinate2.first), std::max(keyCoordinate1.second, keyCoordinate2.second));
    std::pair<int,int> hautGauche(std::min(keyCoordinate1.first, keyCoordinate2.first), std::min(keyCoordinate1.second, keyCoordinate2.second));
    std::pair<int,int> basGauche(std::max(keyCoordinate1.first, keyCoordinate2.first), std::min(keyCoordinate1.second, keyCoordinate2.second));
    std::pair<int,int> basDroit(std::max(keyCoordinate1.first, keyCoordinate2.first), std::max(keyCoordinate1.second, keyCoordinate2.second));
    if (keyCoordinate1 == hautDroit || keyCoordinate1 == basGauche ){
        if (keyCoordinate1.second == hautGauche.second){
            image += (key[basDroit.first][basDroit.second]);
            image += (key[hautGauche.first][hautGauche.second]);
        } else {
            image += (key[hautGauche.first][hautGauche.second]);
            image += (key[basDroit.first][basDroit.second]);
        }
    }
    else {
        if (keyCoordinate1.second == hautDroit.second){
            image += (key[basGauche.first][basGauche.second]);
            image += (key[hautDroit.first][hautDroit.second]);
        } else {
            image += (key[hautDroit.first][hautDroit.second]);
            image += (key[basGauche.first][basGauche.second]);
        }
    }

    return image;
}

std::string playFair::chiffreText(std::string text, key K) {
    std::string textAChiffre = text;

    for (int i = 0; i < textAChiffre.size(); i += 2) {

        if (textAChiffre[i] == textAChiffre[i+1]){
            textAChiffre.insert( i+1, "X", 1);
        }
    }
    if (textAChiffre.size()%2 > 0)
        textAChiffre += 'X';

    std::string textChiffre;
    for(int i = 0 ; i < textAChiffre.size() ; i += 2){
        std::string str = textAChiffre.substr(i, 2);
        textChiffre += chiffrePair(str, K.ckey);
    }

    return textChiffre;
}

std::string playFair::dechiffreText(std::string text, key K) {
    std::string textADechiffre = text;
    std::string textDechiffre;
    for(int i = 0 ; i < textADechiffre.size() ; i += 2){
        std::string str = textADechiffre.substr(i, 2);
        std::string revstr;
        textDechiffre += chiffrePair(str, K.dkey);
    }

    if (textDechiffre.size()%2 == 0 && textDechiffre[textDechiffre.size() - 1] == 'X')
        textDechiffre.erase(textDechiffre.size() - 1 );

    for (int i = 0; i < textDechiffre.size(); ++i) {
        if (textDechiffre[i] == 'X' && textDechiffre[i-1] == textDechiffre[i+1]){
            textDechiffre.erase(i, 1);
            i--;
        }
    }

    return textDechiffre;
}


