# INFO910-Crypto1

Codé par QUENTIN CODELUPI et ALEXANDER PEREZ

## Avancement

Calcul de score - Fonctionnel

Playfaire - Fonctionnel

Craque - Execution Fonctionnel, résultat approximatif

## Fonctionnement

Dans cette version, lors de l'execution, on initialise un calcul de score pour du francais en taille de gram 4.
On affiche dans la console les scores des 4 tests du sujet (SALUT, ATILU, LOREMIPSUM, QIRSOPBBMH).

On initialise aussi un chiffrement/dechiffrement PlayFair avec la clé
```
             'T' 'Q' 'O' 'H' 'W'
             'E' 'I' 'L' 'C' 'S'
             'K' 'U' 'R' 'A' 'G'
             'D' 'F' 'V' 'M' 'N'
             'Y' 'Z' 'B' 'P' 'X'
```

On affiche ensuite le chiffrement/dechiffrement de 2 textes cours.

Pour finir on lance la fonction de craque qui prend un temps relativement long pou s'executer ( quelques minutes)

## Compilation / Lancement

